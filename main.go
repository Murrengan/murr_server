package main

import (
	"fmt"
	"github.com/rs/cors"
	"net/http"
)

func main() {
	fmt.Println("murr_server запущен")
	mux := http.NewServeMux()
	mux.HandleFunc("/murrengan/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte("{\"message\": \"Привет, муррен! Да пребудет с тобой сила! И о как я люблю настраивать https\"}"))
		fmt.Println("Вызвана функция по роуту murrengan")
	})
	handler := cors.Default().Handler(mux)
	err := http.ListenAndServe(":1991", handler)
	if err != nil {
		fmt.Println("murr_server упал:", err)
	}
}

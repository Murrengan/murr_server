FROM golang:1.17-alpine

WORKDIR /

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY *.go ./

RUN go build -o /murr_server

EXPOSE 1991


ENTRYPOINT ["/murr_server"]
